#title:        Dockerfile
#description:  Dockerfile for creating CentOS 7 with NVIDIA Cuda 10.0 drivers,
#              Python 3.6.8 with Virtual Environment, and Darknet
#organization: Kajaani University of Applied Sciences (KAMK)
#project:      HPC4RDI project
#author:       Jukka Jurvansuu <jukka.jurv@nsuu.fi>
#created:      2019-08-05
#modified:     2019-08-06
#version:      1.0
#usage:        nvidia-docker run -d kamkhub/centos7-nvidia-gpu-darknet-python368 /usr/sbin/init
#image:        kamkhub/centos7-nvidia-gpu-darknet-python368
#==============================================================================

FROM nvidia/cuda:10.0-cudnn7-devel-centos7
LABEL maintainer="Jukka Jurvansuu jukka.jurvansuu@kamk.fi"

# Log start time
RUN \
  date

# Update CentOS and install some tools
RUN \
  yum update -y && \
  yum install -y --setopt=tsflags=nodocs \
    bzip2 \
    epel-release \
    hostname \
    iproute \
    openssh-clients \
    patch \
    rsync \
    telnet \
    unzip \
    wget

# Install Rsyslog
RUN \
  yum install --setopt=tsflags=nodocs -y \
  rsyslog

# Enable services
RUN \
  systemctl enable rsyslog.service

# Install Development Tools
RUN \
  yum groups install -y --setopt=tsflags=nodocs \
    "Development Tools"

#==============================================================================
# Python installation
#==============================================================================

# Install Python 3.6.8
# Original location: https://www.python.org/ftp/python/3.6.8/Python-3.6.8.tgz
RUN \
  yum install -y --setopt=tsflags=nodocs \
    libffi-devel \
    libSM \
    libuuid-devel \
    libXext \
    libXrender \
    openssl-devel && \
  cd && \
  mkdir -p python && \
  cd python && \
  wget https://gitlab.com/kamkhub/hpc/docker/kamkhub-hpc-docker-centos7-nvidia-gpu-darknet-python368/raw/master/res/python/Python-3.6.8.tgz && \
  tar -xf Python-3.6.8.tgz && \
  rm -f Python-3.6.8.tgz && \
  cd Python-3.6.8 && \
  ./configure --enable-optimizations --prefix=/opt && \
  make && \
  make install && \
  cd

# Install Virtual Environment
RUN \
  yum install -y --setopt=tsflags=nodocs \
    python-virtualenv && \
  virtualenv --no-site-packages -p /opt/bin/python3 /opt/venv/python3

# Activate Python 3.6.8
# Info: source /opt/venv/python3/bin/activate

# Install required modules using PIP
RUN \
  source /opt/venv/python3/bin/activate && \
  pip install --upgrade pip && \
  pip install numpy tensorflow-gpu && \
  pip install Keras scipy scikit-image && \
  pip install watchdog && \
  pip install pyinotify && \
  pip install asyncio && \
  pip install opencv-python && \
  # yum install -y sqlite-devel
  # pip install pysqlite3
  yum install -y mariadb mariadb-devel && \
  pip install mysqlclient && \
  pip install python-daemon-3K && \
  pip install ConfigParser && \
  pip install psutil

# Info: Testing
# source /opt/venv/python3/bin/activate
# python --version
# deactivate
# python --version

#==============================================================================
# Darknet installation
# https://pjreddie.com/darknet/
#==============================================================================

# Install Darknet
# Original location: https://github.com/pjreddie/darknet/archive/master.zip
# NVIDIA K80: sed -i '/^ARCH=/s/-gencode.*/-gencode arch=compute_35,code=sm_35/' Makefile && \
# NVIDIA V100: sed -i '/^ARCH=/s/-gencode.*/-gencode arch=compute_70,code=sm_70/' Makefile && \

RUN \
  cd && \
  wget https://gitlab.com/kamkhub/hpc/docker/kamkhub-hpc-docker-centos7-nvidia-gpu-darknet-python368/raw/master/res/darknet/darknet-master.zip && \
  unzip darknet-master.zip && \
  cd darknet-master && \
  sed -i '/^GPU=/s/0/1/' Makefile && \
  sed -i '/^ARCH=/s/-gencode.*/-gencode arch=compute_70,code=sm_70/' Makefile && \
  sed -i '/^[# \t]*-gencode/d' Makefile && \
  make && \
  cp libdarknet.so /opt/lib/

# Test Darknet
# cd
# cd darknet-master
# ./darknet imtest data/eagle.jpg

# Clean YUM caches to minimise Docker image size...
RUN \
  yum clean all && rm -rf /tmp/yum*

# Log end time
RUN \
  date

CMD ["/usr/sbin/init"]
